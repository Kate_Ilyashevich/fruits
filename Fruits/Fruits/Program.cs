﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Fruits
{
    class Basket : IEnumerable
    {
        private string[] fruits = { "Apple", "Pear", "Pineapple", "Banana", "Peach", "Orange", "Blackberry" };
        public IEnumerator GetEnumerator()
        {
            for (int index = 0; index <= fruits.Length / 2; index++)
            {
                yield return fruits[index];
                if (fruits.Length - 1 - index != index)
                    yield return fruits[fruits.Length - 1 - index];
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Basket fruits = new Basket();

            foreach (string fruit in fruits)
            {
                Console.Write(fruit + " ");
            }

            Console.ReadKey();
        }
    }
}
